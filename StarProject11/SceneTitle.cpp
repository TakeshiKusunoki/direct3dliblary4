//******************************************************************************
//
//
//		タイトル処理
//
//
//******************************************************************************

#include "All.h"

//******************************************************************************
//
//		初期設定
//
//******************************************************************************

void SceneTitle::init(UINT i )
{
	timer = 0;
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\Title.png");
			pMusic->musicPlay(ENUM::TITLE, true);
			break;
		default:
			break;
	}


}

//******************************************************************************
//
//		更新処理
//
//******************************************************************************

void SceneTitle::update(UINT i )
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			timer++;
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			break;
		default:
			break;
	}
	//セレクト画面へ
	if (input::TRG(0) & input::PAD_START || input::TRG(0) & input::PAD_TRG1 || input::TRG(0) & input::PAD_TRG2 || input::TRG(0) & input::PAD_TRG3) {
		//タイトルBGMを止める
		pMusic->musicStop(ENUM::TITLE);
		//タイトルから進むSE
		pMusic->soundPlay(12);
		setScene(pSceneSelect);

	}
	if (GetAsyncKeyState('Z') < 0 || GetAsyncKeyState('X') < 0 || GetAsyncKeyState('C') < 0 || GetAsyncKeyState(VK_SPACE) < 0)
	{
		//タイトルBGMを止める
		pMusic->musicStop(ENUM::TITLE);
		//タイトルから進むSE
		pMusic->soundPlay(12);
		setScene(pSceneSelect);
	}
	if(GetAsyncKeyState('Z')<0 || GetAsyncKeyState('X')<0 || GetAsyncKeyState('C')<0) setScene(pSceneSelect);
	//if (timer > SEC * 5)setScene(pSceneSelect);
	// 5秒後にデモへ切り換え
	//if (timer > 60 * 5)setScene(pSceneDemo);

}

//******************************************************************************
//
//		描画処理
//
//******************************************************************************

void SceneTitle::draw(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
		/*	spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1280, 760, static_cast<float>(WinFunc::GetScreenWidth(i)) / 1280, static_cast<float>(WinFunc::GetScreenHeight(i)) / 760);*/
			spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1280, 760, 1,1);
			break;
		default:
			break;
	}
}

//******************************************************************************