
#define DEVELOP// 開発中（デモ作成）
#include "All.h"
//ラベル定義
#define DEMO_SAVE_MAX (60*60)
#define DEMO_FILE "demo_data.txt"
char x[] = "demo_data.txt";

#ifdef DEVELOP	//デモ作成の時
int demoData[DEMO_SAVE_MAX];
#else		//でも生成の時
int demoData[] = {
#include DEMO_FILE//デモデータをここに読み込む
	-1	//終了コード
};
#endif

void SceneDemo::init() {
	timer = 0;
	pStageManager->init();
}

void SceneDemo::update() {

#ifdef DEVELOP	//デモ作成の時
	// デモ作成
	//demoData[timer] = pad_state;
	//if (timer == 0) pad_trg = 0;
	//timer++;

	//// 終了チェック
	//if ((pad_trg & PAD_START) || (timer == DEMO_SAVE_MAX)) {
	//	FILE	*fp;
	//	fopen_s(&fp, DEMO_FILE, "wt");					// ファイルオープン（fopenのセキュリティ強化版）
	//	for (int i = 0; i < timer; i++) {
	//		if ((i % 8) == 0) fprintf(fp, "	");			// 8個単位の先頭にスペースを挿入
	//		fprintf(fp, "0x%08x, ", demoData[i]);
	//		if ((i % 8) == 7) fprintf(fp, "\n");		// 8個単位の最後に改行を挿入
	//	}
	//	fclose(fp);										// ファイルクローズ
	//	setScene(pSceneTitle);
	//	return;
	//}

#else
	// キー入力による終了チェック
	if (pad_trg & PAD_START) {
		setScene(pSceneTitle);
		return;
	}

	// デモ再生
	pad_state = demoData[timer];
	pad_trg = (timer) ? pad_state & ~demoData[timer - 1] : 0;

	// 終了チェック
	timer++;
	if (demoData[timer] == -1) {
		setScene(pSceneTitle);
		return;
	}

#endif
	pStageManager->update();
}


void SceneDemo::draw() {
	pStageManager->draw();
#ifdef DEVELOP      // デモ作成のとき
	//if (timer & 0x20) GL::DrawStringM(320, 180, "DEMO作成モード", COLOR_CYAN, true);
	//GL::DrawStringS(0, 440, "通常にプレイ");
	//GL::DrawStringS(0, 460, "[SPACE]で保存");
#else        // デモ再生のとき
	//if (timer & 0x20) GL::DrawStringM(320, 180, "DEMO", COLOR_CYAN, true);
#endif
}
