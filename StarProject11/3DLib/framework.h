#pragma once

#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く

#include <windows.h>
#include <tchar.h>
#include <sstream>

#include "misc.h"
//#include "high_resolution_timer.h"

#include <d3d11.h>//追加
#include <dxgi.h>
//#include "sprite2D.h"
#include "Blend.h"
#include "Font.h"
#include "GeometricPrimitive.h"
// unit12
#include "Static_mesh.h"
// UNIT.16
#include "Skinned_mesh.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")



#define WINDOW_NUM 3

#define THREAD_NUM_MAX 1//7個まで
class ResourceManager;
class BlendMode;
class framework
{
private:
	IDXGIFactory* p_Factory;//スワップチェイン
	//ID3D11CommandList* p_CommandList[THREAD_NUM_MAX];
	BlendMode* p_Blend;
	//high_resolution_timer timer;
public:
	HWND hwnd[WINDOW_NUM];
	UINT SCREEN_WIDTH[WINDOW_NUM];
	UINT SCREEN_HEIGHT[WINDOW_NUM];
	//static CONST BYTE  THREAD_NUM_MAX_ = THREAD_NUM_MAX;
	////////////////////////////////////////
	//Added by Unit7
	///////////////////////////////////////
	ID3D11Device* p_Device;//デバイス
	ID3D11DeviceContext* p_ImidiateContext;//イミィディエイトコンテキスト
	IDXGISwapChain* p_SwapChain[WINDOW_NUM];
	ID3D11RenderTargetView* p_RenderTargetView[WINDOW_NUM];
	ID3D11DepthStencilView* p_DepthStencilView[WINDOW_NUM];
	//ID3D11DeviceContext* p_DeferredContext[THREAD_NUM_MAX];//遅延コンテキスト
	//ID3D11BlendState* p_BlendState;

	////////////////////////////////////////
	//Added by Unit10
	///////////////////////////////////////

	/*GeometricPrimitive *cube;
	Static_mesh* mesh[THREAD_NUM_MAX];
	Skinned_mesh* skin[2];*/

	////////////////////////////////////////
	//Added by Unit9
	///////////////////////////////////////
	//LONGLONG CountsPerSecond;
	//LONGLONG CountsPerFrame;//1/60秒
	//LONGLONG NextCounts;

public:
	bool initialize();
	////////////////////////////////////////
	//Castamed by Unit8
	///////////////////////////////////////
	framework(HWND hwnd_[], UINT SCREEN_WIDTH_[], UINT SCREEN_HEIGHT_[]) :
		p_Device(nullptr),p_ImidiateContext(nullptr), p_Factory(nullptr)/*,particle(nullptr)*/
	{
		for (int i = 0; i < WINDOW_NUM; i++)
		{
			SCREEN_WIDTH[i] = SCREEN_WIDTH_[i];
			SCREEN_HEIGHT[i] = SCREEN_HEIGHT_[i];
			hwnd[i] = hwnd_[i];
			p_SwapChain[i] = nullptr;
			p_RenderTargetView[i] = nullptr;
			p_DepthStencilView[i] = nullptr;
		}
		/*for (int i = 0; i < THREAD_NUM_MAX; i++)
		{
			p_DeferredContext[i] = nullptr;
			p_CommandList[i] = nullptr;
		}*/

	}
#define RELEASE_IF(x) if(x){x->Release();x=nullptr;}
#define DELETE_IF(x) if(x){delete x; x=NULL;}
	~framework()
	{
		// デバイスステートのクリア
		if (p_ImidiateContext)p_ImidiateContext->ClearState();


		/*DELETE_IF(cube);
		DELETE_IF(skin[0]);
		DELETE_IF(skin[1]);
		for (int i = 0; i <THREAD_NUM_MAX; i++)
		{
			if (p_DeferredContext[i])p_DeferredContext[i]->ClearState();
			DELETE_IF(mesh[i]);
			RELEASE_IF(p_DeferredContext[i]);
			RELEASE_IF(p_CommandList[i]);
		}*/

		Font::ReleaseImage();
		RELEASE_IF(p_Blend);
	    ////////////////////////////////////////
	    //Added by Unit7
	    ///////////////////////////////////////
		// 取得したインターフェースのクリア

		for (UINT i = 0; i < WINDOW_NUM; i++)
		{
			RELEASE_IF(p_DepthStencilView[i]);
			RELEASE_IF(p_RenderTargetView[i]);
			RELEASE_IF(p_SwapChain[i]);
		}
		RELEASE_IF(p_Factory);
		RELEASE_IF(p_ImidiateContext);
	    RELEASE_IF(p_Device);
	}
#undef RELEASE_IF
#undef DELETE_IF
//	int run();
//
//
//	//LRESULT CALLBACK handle_message(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
//	//{
//	//	switch (msg)
//	//	{
//	//	case WM_PAINT:
//	//	{
//	//		PAINTSTRUCT ps;
//	//		HDC hdc;
//	//		hdc = BeginPaint(hwnd, &ps);
//	//		EndPaint(hwnd, &ps);
//	//		break;
//	//	}
//	//	case WM_DESTROY:
//	//		PostQuitMessage(0);
//	//		break;
//	//	case WM_CREATE:
//	//		break;
//	//	case WM_KEYDOWN:
//	//		if (wparam == VK_ESCAPE) PostMessage(hwnd, WM_CLOSE, 0, 0);
//	//		break;
//	//	case WM_ENTERSIZEMOVE:
//	//		// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
//	//		timer.stop();
//	//		break;
//	//	case WM_EXITSIZEMOVE:
//	//		// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
//	//		// Here we reset everything based on the new window dimensions.
//	//		timer.start();
//	//		break;
//	//	default:
//	//		return DefWindowProc(hwnd, msg, wparam, lparam);
//	//	}
//	//	return 0;
//	//}
//
//private:
//
//	void update(float elapsed_time/*Elapsed seconds from last frame*/);
//	void render(float elapsed_time/*Elapsed seconds from last frame*/);
//	// 無駄な画面描画の抑制
//	bool StanbyPresant(float elapsed_time);//! 追加
//	// マルチスレッド
//	void ThreadFunc(Static_mesh** mesh, ID3D11CommandList ** ppCommandList, ID3D11DeviceContext * pDeferredContext, const DirectX::XMFLOAT4X4 & wvp, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & lightVector, const DirectX::XMFLOAT4 * materialColor, bool FlagPaint);
//
	//void calculate_frame_stats()
	//{
	//	// Code computes the average frames per second, and also the
	//	// average time it takes to render one frame.  These stats
	//	// are appended to the window caption bar.
	//	static int frames = 0;
	//	static float time_tlapsed = 0.0f;

	//	frames++;

	//	// Compute averages over one second period.
	//	if ((timer.time_stamp() - time_tlapsed) >= 1.0f)
	//	{
	//		float fps = static_cast<float>(frames); // fps = frameCnt / 1
	//		float mspf = 1000.0f / fps;
	//		std::ostringstream outs;
	//		outs.precision(6);
	//		outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
	//		SetWindowTextA(hwnd, outs.str().c_str());

	//		// Reset for next average.
	//		frames = 0;
	//		time_tlapsed += 1.0f;
	//	}
	//}
};
