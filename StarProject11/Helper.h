#pragma once

///////////////////////////////////
//	MoveAlg
///////////////////////////////////
class MoveAlgHelper
{
public:
	virtual void move(HELPER_DATA* Helper, const OBJ3D* obj) { Helper; obj; }
protected:
	//new動的確保
	//arrayNum : 配列数
	//この時生成した配列数より大きい数を、後でHELPER->dataNumに入れることはできない
	void createNew(HELPER_DATA* Helper, int arrayNum = 1);
};

class EraseAlgHelper
{
public:
	virtual void erace(HELPER_DATA* Helper) = 0;
};



///////////////////////////////////
//	HELPERデータ
///////////////////////////////////
class HELPER_DATA
{
protected:
	//フラグ系
	struct FLAG
	{
		bool active;//活動
	};

public:
	MoveAlgHelper*                mvAlg;              // 移動アルゴリズム
	EraseAlgHelper*               eraseAlg;           // 消去アルゴリズム

	MyMesh Model;//モデルオブジェクト;
	DirectX::XMFLOAT3 position;//位置
	DirectX::XMFLOAT3 scale;//大きさ
	DirectX::XMFLOAT3 angle;//回転角度
	DirectX::XMFLOAT4 color;//色

	int state;
	int timer;

	// ローカル構造体---------------------------------
	FLAG Flag;




public:
	HELPER_DATA();        // コンストラクタ
	void clear();   // メンバ変数のクリア
	//オブジェクトのアップデートで呼び出すmove関数
	void moveObj(const OBJ3D* obj);
	// 描画
	void draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);
	// 当たり判定描画
	//void drawHitRect(const VECTOR4&, int i);
	//i番目の画像のアニメーションをアップデート
	//bool animeUpdate(int i);
};



class SphireMove : public MoveAlgHelper
{
public:
	void move(HELPER_DATA* Helper, const OBJ3D* obj);
};


//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================
class EraseHelper: public EraseAlgHelper
{
public:
	void erace(HELPER_DATA* Helper);
};
EXTERN EraseHelper eraseHelper;







///////////////////////////////////
//	オブジェクト付随HELPER
///////////////////////////////////
class HelperManager
{
protected:
	std::list<HELPER_DATA>  helperList; // リスト（HELPER_DATAの配列のようなもの）
public:
	void init();
	void update(const OBJ3D* obj);
	void draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);
	// 終了処理
	void UnInit();
	//引数
	//移動関数
	//位置
	//aiフラグ
	 //大きさ
	 //回転角度
	 //色
	HELPER_DATA* add(MoveAlgHelper* mvAlgHelper,
			DirectX::XMFLOAT3 position = { 0,0,0 },//位置
			DirectX::XMFLOAT3 scale = { 1,1,1 },	//大きさ
			DirectX::XMFLOAT3 angle = { 0,0,0 },	//回転角度
			DirectX::XMFLOAT4 color = { 1,1,1,1 }	//色
	); // HELPERListに新たなHELPERDATAを追加する
	std::list<HELPER_DATA>* getList() { return &helperList; }                //  HelperListを取得する
};