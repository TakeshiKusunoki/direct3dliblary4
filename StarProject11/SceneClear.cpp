//******************************************************************************
//
//
//		ゲームクリア処理
//
//
//******************************************************************************

#include "All.h"

//******************************************************************************
//
//		初期設定
//
//******************************************************************************

void SceneClear::init(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\Result1.png");
			//勝ちリザルトBGM
			pMusic->musicPlay(ENUM::RESULT1, true);
			break;
		default:
			break;
	}
	timer = 0;
}

//******************************************************************************
//
//		更新処理
//
//******************************************************************************

void SceneClear::update(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			timer++;
			if (timer >= 0x160 - 1)
			{
				//リザルトBGMを止める
				pMusic->musicStop(ENUM::RESULT1);
				setScene(pSceneTitle);
			}
			break;
		default:
			break;
	}

	//if ((timer > 0x40) && (pad_trg)) setScene(pSceneTitle);
}

//******************************************************************************
//
//		描画処理
//
//******************************************************************************

void SceneClear::draw(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			//spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0,1280, 760, static_cast<float>(WinFunc::GetScreenWidth(i)) / 1280, static_cast<float>(WinFunc::GetScreenHeight(i)) / 760);
			spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1280, 760, 1,1);
			break;
		default:
			break;
	}
				// GL::ClearScene();
	//if (timer & 0x20) GL::DrawStringL(320, 160, "GAME CLEAR", COLOR_CYAN, true);
}

//******************************************************************************