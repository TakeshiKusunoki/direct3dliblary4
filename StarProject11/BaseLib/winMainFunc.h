#pragma once
#define WIN32_LEAN_AND_MEAN		// ヘッダーからあまり使われない関数を省く

#include <windows.h>
#include "high_resolution_timer.h"
#define WINDOW_NUM 3//ウインドウの数

namespace Enum {
	enum WINDOW_NUMBER
	{
		WINDOW_0 = 0,
		WINDOW_1,
		WINDOW_2,
	};
}

// プロトタイプ宣言
//BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);        // ダイアログウィンドウのコールバック関数
//ウィンドウプロシジャ関数
namespace WinFunc
{
	LRESULT CALLBACK WinProc(HWND, UINT, WPARAM, LPARAM);
	LRESULT CALLBACK WinProc2(HWND, UINT, WPARAM, LPARAM);
	LRESULT CALLBACK WinProc3(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	BOOL InitWindow(HINSTANCE hInstance, int nShowCmd);
	BOOL InitInstance(HINSTANCE hInstance, int nShowCmd);
	BOOL InitInstance2(HINSTANCE hInstance, int nShowCmd);
	BOOL InitInstance3(HINSTANCE hInstance, int nShowCmd);
	BOOL InitWindow2(HINSTANCE hInstance, int nShowCmd);
	BOOL InitWindow3(HINSTANCE hInstance, int nShowCmd);
	BOOL Console();

	void Uninit();				// ウインドウ終了処理
	bool MsgLoop();				// メッセージ処理

	//その他------------------------------
	//引数　: ウィンドウ画面番号
	HWND GetHwnd(UINT i = 0);
	//SCREEN_WIDTH
	UINT GetScreenWidth(UINT i = 0);
	//SCREEN_HEIGHT
	UINT GetScreenHeight(UINT i = 0);

	high_resolution_timer* GetElapsedTimer(UINT i = 0);
	//void  Calculate_frame_stats(UINT i = 0);
	void CursorOn();			// カーソル表示
	void CursorOff();			// カーソル消去
	void UpdateCursorPos(UINT i);		// カーソル座標の更新
	POINT GetCursorPos();		// カーソル座標（クライアント座標）を返す
}
